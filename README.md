# processor

Многопоточная обработка очереди с учетом последовательной обработки в рамках groupId элемента
Число groupId для элементов по умолчанию 5

#Сборка проекта

mvn clean package

#Запуск

cd ./target/build
java -cp lib/*:ext/*:etc/* -Dthread.count=5 -Dlog4j.configuration=file:./etc/log4j.properties  ru.tetal.Application

