package ru.tetal;

import org.apache.log4j.Logger;
import ru.tetal.model.Item;
import ru.tetal.service.ItemsGenerator;
import ru.tetal.service.ItemsProcessor;
import ru.tetal.service.beans.ItemGeneratorBean;
import ru.tetal.service.beans.ItemsProcessorBean;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Created by user on 02.05.2018.
 */
public class Application {

    private static final Logger logger = Logger.getLogger(Application.class);

    private static final AtomicBoolean running = new AtomicBoolean(true);

    public static void main(String[] args) throws InterruptedException {

        String threadCountParam=System.getProperty("thread.count");
        Integer count;

        if (threadCountParam==null) {
            logger.info("start process with default thread size:5");
            count=5;
        }
        else{
            count=Integer.valueOf(threadCountParam);
            logger.info("start process with thread size:"+count);
        }

        BlockingQueue<Item> incoming=new LinkedBlockingQueue<Item>();
        ItemsProcessor processor=new ItemsProcessorBean(incoming,count);
        ItemsGenerator generator=new ItemGeneratorBean();

        Runtime.getRuntime().addShutdownHook(new Thread()
        {
            @Override
            public void run()
            {
                logger.info("Shutdown...");
                running.set(false);
                processor.stop();
                while (!processor.isStoped()){
                    logger.info("wait 100ms while process stopped..");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        logger.error(e);
                    }
                }
                logger.info("incoming last size:"+incoming.size());
            }
        });

        processor.start();
        while (running.get()){
            incoming.put(generator.getItem());
            Thread.sleep(200L);
        }
    }
}
