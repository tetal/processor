package ru.tetal.model;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by user on 02.05.2018.
 */
@Data
@AllArgsConstructor
public class Item {
    long groupId;
    long itemId;
}
