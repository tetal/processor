package ru.tetal.service;

/**
 * Created by user on 02.05.2018.
 */
public interface ItemsProcessor {
    public void start();
    public void stop();
    public boolean isStoped();
}
