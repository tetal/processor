package ru.tetal.service.beans;

import org.apache.log4j.Logger;
import ru.tetal.model.Item;
import ru.tetal.service.ItemsGenerator;

/**
 * Created by user on 02.05.2018.
 */
public class ItemGeneratorBean implements ItemsGenerator {

    private static final Logger logger = Logger.getLogger(ItemGeneratorBean.class);

    long idSeq=0;
    long groupIdMaxSize=5; //default value 5;
    public ItemGeneratorBean(){};
    public ItemGeneratorBean(long groupIdMaxSize){
        this.groupIdMaxSize=groupIdMaxSize;
    }
    public Item getItem() {
        Item item=new Item((long)(Math.random() * groupIdMaxSize) + 1,++idSeq);
        logger.info("Item generate:"+item);
        return item;
    }
}
