package ru.tetal.service.beans;

import org.apache.log4j.Logger;
import ru.tetal.model.Item;
import ru.tetal.service.ItemsProcessor;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Created by user on 03.05.2018.
 */
public class ItemsProcessorBean implements ItemsProcessor {

    private static final Logger logger = Logger.getLogger(ItemsProcessorBean.class);

    BlockingQueue<Item> incoming;

    private int threadCount;

    private Thread preProcessThread;

    protected Map<BlockingQueue<Item>,Thread> threadMap=new HashMap<>();

    private Map<Long,BlockingQueue<Item>> queueMap=new HashMap<>();

    protected static final long POLL_TIMEOUT=100L;

    private final AtomicBoolean runningPreProcessThread = new AtomicBoolean(true);
    private final AtomicBoolean runningProcessThread = new AtomicBoolean(true);


    private ItemsProcessorBean(){};

    public ItemsProcessorBean(BlockingQueue<Item> incoming,int threadCount) {
        this.incoming=incoming;
        this.threadCount=threadCount;
    }

    @Override
    public void start() {
        preProcessThread =new Thread(()->{
            while (runningPreProcessThread.get()){
                try {
                    Item item = incoming.poll(POLL_TIMEOUT,TimeUnit.MILLISECONDS);
                    logger.debug("take item:"+item);
                    if (item!=null) {
                        BlockingQueue<Item> queue=itemRoute(item);
                        queue.put(item);
                        itemPreProcess(queue);
                    }
                }catch (InterruptedException e){
                    logger.error(e);
                }
            }
            logger.info(Thread.currentThread().getName()+" is stoped");
        });
        preProcessThread.setName("itemPreProcess Thread");
        preProcessThread.start();
    }

    private BlockingQueue<Item> itemRoute(Item item){
        if (queueMap.containsKey(item.getGroupId())){
            return queueMap.get(item.getGroupId());
        }
        else if (queueMap.size()<threadCount){
            BlockingQueue<Item> queue=new LinkedBlockingQueue<>();
            queueMap.put(item.getGroupId(),queue);
            return queue;
        }
        else{
            Long key = queueMap.entrySet().stream().collect(Collectors.groupingBy(entry -> entry.getKey(),
                    Collectors.counting())).entrySet().stream().
                    sorted(Map.Entry.comparingByValue()).findFirst().get().getKey();
            BlockingQueue<Item> queue = queueMap.get(key);
            queueMap.put(item.getGroupId(),queue);
            return queue;
        }


    }

    private void itemPreProcess(BlockingQueue<Item> queue){
        if (!threadMap.containsKey(queue)){
            Thread thread=new Thread(()->{
                while (true){
                    try {
                        Item item = queue.poll(POLL_TIMEOUT, TimeUnit.MILLISECONDS);
                        if (item!=null) itemProcess(item);
                        if (!runningProcessThread.get() && queue.isEmpty()){
                            break;
                        }
                    } catch (InterruptedException e) {
                        logger.error(e);
                    }
                }
                logger.info(Thread.currentThread().getName()+" is stopped");
            });
            threadMap.put(queue,thread);
            thread.start();
        }

    };

    @Override
    public void stop() {
        runningPreProcessThread.set(false);
        logger.debug("set runningPreProcessThread: "+ runningPreProcessThread.get());
        while(queueMap.values().stream().map(queue -> queue.size()).mapToInt(Number::intValue).sum()!=0){
            logger.debug("wait 100ms while process end..");
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                logger.error(e);
            }
        }
        runningProcessThread.set(false);
        logger.debug("set runningProcessThread: "+ runningProcessThread.get());
    }

    protected void itemProcess(Item item){
        logger.info("item process:"+item);
        try {
            Thread.sleep(200+(long)(Math.random() * 200));
        } catch (InterruptedException e) {
           logger.error(e);
        }
    }
    @Override
    public boolean isStoped(){
        boolean isStoped=true;
        isStoped=!preProcessThread.isAlive();
        for(Thread thread:threadMap.values()){
            if (thread.isAlive()) isStoped=false;
            break;
        }
        return isStoped;


    }
}
