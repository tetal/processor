package ru.tetal.service;

import ru.tetal.model.Item;

/**
 * Created by user on 02.05.2018.
 */
public interface ItemsGenerator {
    public Item getItem();
}
